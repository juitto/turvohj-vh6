#include <iostream>
#include <filesystem>

using namespace std;

class MyArray {
public:
	// For bookkeeping.
	static int countNew;
	static int countDelete;

	MyArray( int size, int initValue = 0 )
	:	mLength( 0 ),
		mValues( nullptr )
	{
		try {
			mValues = new int[ size ];
			countNew++;
		}
		catch( ... )
		{
			throw "Memory allocation failed";
		}
		mLength = size;
		for( int i = 0; i < size; i++ )
			mValues[ i ] = initValue;
	}

	// Copy constructor.
	MyArray( const MyArray& other )
	:	mLength( 0 ),
		mValues( nullptr )
	{
		this->operator=( other );
	}

	// Assign operator.
	MyArray& operator=( const MyArray& other )
	{
		// Do not assign to my self.
		if( this != &other )
		{
			this->~MyArray();
			try {
				mValues = new int[ other.mLength ];
				countNew++;
			}
			catch( ... )
			{
				throw "Memory allocation failed";
			}
			mLength = other.mLength;
			for( int i = 0; i < mLength; i++ )
				mValues[ i ] = other.mValues[ i ];
		}
		return *this;
	}

	~MyArray()
	{
		if( mValues )
		{
			countDelete++;
			delete[] mValues;
			mLength = 0;
			mValues = nullptr;
		}
	}

	void Print()
	{
		for( int i = 0; i < mLength; i++ )
			cout << mValues[ i ] << ( mLength - 1 == i ? "" : ", " );
		cout << endl;
	}

	int& At( int i )
	{
		if( i < 0 || i >= mLength )
			throw "Out of bounds";
		return mValues[ i ];
	}

	static MyArray Concat( const MyArray& a1, const MyArray& a2 )
	{
		// newSize can overflow integer.
		int newSize = a1.mLength + a2.mLength;
		MyArray output( newSize );
		for( int i = 0; i < a1.mLength; i++ )
			output.At( i ) = a1.mValues[ i ];
		for( int i = 0; i < a2.mLength; i++ )
			output.At( a1.mLength + i ) = a2.mValues[ i ];

		return output;
	}

// Private member variables.
private:
	int mLength;
	int* mValues;
};

int MyArray::countNew = 0;
int MyArray::countDelete = 0;

int main( int argc, char **argv )
{
	cout << endl << endl << "T2" << endl;

	// Basic tests.
	{
		MyArray arr1( 10 );
		arr1.Print();
		arr1.At( 3 ) = 2;
		arr1.Print();
		cout << "----------" << endl;

		MyArray arr2( arr1 );
		MyArray arr3( 0 );
		arr3 = arr2;
		arr2.Print();
		arr3.Print();
		cout << "----------" << endl;

		arr2.At( 1 ) = 3;
		arr1.Print();
		arr2.Print();
		arr3.Print();
		cout << "----------" << endl;
	}

	// Out of bounds test.
	{
		MyArray arr( 5, 5 );
		arr.Print();
		try {
			arr.At( 6 ) = 2;
		}
		catch( const char* err )
		{
			cout << err << endl;
		}
		cout << "----------" << endl;
	}

	// Concat test.
	{
		MyArray arr1( 7, 1 );
		MyArray arr2( 5, 4 );
		arr1.Print();
		arr2.Print();
		cout << "----------" << endl;

		MyArray arr3( MyArray::Concat( arr1, arr2 ) );
		MyArray arr4 = MyArray::Concat( arr1, arr2 );
		arr1.Print();
		arr2.Print();
		arr3.Print();
		arr4.Print();
		cout << "----------" << endl;

		arr1 = MyArray::Concat( arr1, arr2 );
		arr1.Print();
		arr2.Print();
 		cout << "----------" << endl;
	}
	cout << "CountNew: " << MyArray::countNew << endl;
	cout << "CountDel: " << MyArray::countDelete << endl;

	return 0;
}
